// Step 1 : Http server
const express = require("express"); 
const app = express();
var http = require("http").createServer(app);

// Step 2 : MongoDb database 
var mongodb = require("mongodb"); 
var ObjectId = mongodb.ObjectId;
var mongoClient = mongodb.MongoClient;
               
var mainURL = "http://localhost:3000/";
var database = null;

// Step 3 : home page 
app.use("/public", express.static(__dirname + "/public"));
  app.set ("view engine", "ejs");
  app.use(express.json());

  
// Step 4 : express-session for login and regestration
var expressSession = require("express-session"); 
app.use(expressSession({
    "key": "user_id",
    "secret": "User secret object ID",
    "resave": true,
    "saveUninitialized": true
}));

// Step 5 : get form fealds (regestration and login)
var bodyParser = require("body-parser");
app.use (bodyParser.json( { limit: "10000mb" } ));
app.use (bodyParser.urlencoded ( { extended: true, limit: "10000mb", parameterLimit: 1000000 } ));

// Step 6 : convert plain password into encrypted string
var bcrypt = require("bcrypt");

// Step 7 : formidable and FS module to save files to Node JS server directory
var formidable = require("formidable");
var fileSystem = require("fs");
                     
////////////////////////////////////////////////////////////

// Get user form database
function getUser(userId, callBack) {
  database.collection("users").findOne({
      "_id": ObjectId(userId)
 }, function (error, result) {
      if (error) {
          console.log(error);
          return;
      }
      if (callBack != null) {
          callBack(result);
      }
  });
}

////////////////////////////////////////////////////////////

// Start server 
http.listen(process.env.PORT || 3000, function () {
  console.log("Connected");

  // connect database 
  mongoClient.connect("mongodb://localhost:27017", {useUnifiedTopology : true}, function(error, client){
    if (error){
      console.log(error);
      return;
    }
    database = client.db("image_sharing_app");

    // render home page
    app.get("/", function(request, result) {

      // view uploaded pictures
      database.collection("images").find().sort({
        "createdAt": -1
      }).toArray(function (error1, images) {
          // view page 
          if (request.session.user_id) {
            getUser(request.session.user_id, function (user) {
                  result.render("index", {
                      "isLogin": true,
                      "query": request.query,
                      "user": user,
                      "images": images
                });
            });
          } else {
            result.render("index", {
              "isLogin" : false,
              "query": request.query,
              "images": images

            });
          }
      });


    });

    // registration page 

        // render page 
    app.get("/register", function(request, result) {
      result.render("register", {
          "query": request.query
      });
    });


    app.get("/panorama", function(request, result) {

           // view uploaded pictures
           database.collection("images").find().sort({
            "createdAt": -1
          }).toArray(function (error1, images) {
              // view page 
              if (request.session.user_id) {
                getUser(request.session.user_id, function (user) {
                      result.render("panorama", {
                          "isLogin": true,
                          "query": request.query,
                          "user": user,
                          "images": images
                    });
                });
              } else {
                result.render("panorama", {
                  "isLogin" : false,
                  "query": request.query,
                  "images": images
    
                });
              }
          });
    

    });

        // register new user and add infos to database 
    app.post ("/register", function (request, result) {
      if (request.body.password != request.body.confirm_password) {
            result.redirect("/register?error=mismatch");
            return;
        }

      database.collection("users").findOne({
        "email": request.body.email     
            }, function (error1, user) {

          if (user == null) {
              bcrypt.hash(request.body.password, 10, function (error3, hash) {
                  database.collection("users").insertOne({
                      "name": request.body.name,
                      "email": request.body.email,
                      "password": hash
                  }, function (error2, data) {
                      result.redirect("/login?message=registered")
                    });
                });
             } else {
              result.redirect("/register?error=exists");
                }
          });
      });

   // login page 
    // render page 
    app.get("/login", function(request, result) {
      result.render("login", {
          "query": request.query
      });
    });

    // handle login request
    app.post("/login", function (request, result) {
        var email = request.body.email;
        var password = request.body.password;

        database.collection("users").findOne({
          "email": email
        }, function (error1, user) {
          if (user == null) {
             result.redirect("/login?error=not_exists");
          } else {
             bcrypt.compare(password, user.password, function (
                 error2, isPasswordVerify) {
                 if (isPasswordVerify) {
                     request.session.user_id = user._id;
                     result.redirect("/");
                  } else {
                     result.redirect("/login?error=wrong_password");
                  }
                });
           }
         }); 
    });


    // logout rederection
      // render page
    app.get("/logout", function (request, result) {
      request.session.destroy();
      result.redirect("/");
      console.log("logout succesful");
    });

    // Users uploaded pictures
    app.get("/my_uploads", function(request, result) {

      if (request. session.user_id) {
          getUser(request.session.user_id, function (user) {

              database.collection("images").find({ 
                "user._id": ObjectId(request.session.user_id)
              }).sort({
                "createdAt": -1
              }).toArray(function (error1, images) {
                  result.render(
                    "index", {
                      "isLogin": true,
                      "query": request.query,
                      "images": images,
                      "user": user
                    });
                });
            });
       } else {
         result.redirect("/login");
       }

     });

      // Upload image in Node JS and save path in Mongo DB 
    app.post("/upload-image", async function (request, result) {
        if (request.session.user_id) {
            var formData = new formidable.IncomingForm();

            formData.maxFileSize = 1000 * 1024 * 1204;

            formData.parse(request, function (error1, fields, files){
                var oldPath = files.image.filepath;
                var newPath = "public/uploads/" + new Date().getTime() + "-" + files.image.originalFilename;
                var imageTitle = fields._title;
                var imageDescription = fields._description;

                console.log(fields._title);
                fileSystem.rename(oldPath, newPath, function (error2) {
                    getUser(request.session.user_id, function (user) {
                        delete user.password;
                        var currentTime = new Date().getTime();

                        database.collection("images").insertOne({
                          "filePath": newPath,
                          "user": user,
                          "createdAt": currentTime,

                          "comments": [],
                          "title": imageTitle, 
                          "description": imageDescription,
                        }, function (error2, data) {
                          result.redirect("/?message=image_uploaded");
                        });
                      });
                 });
              });
          } else {
            result.redirect("/login");
          }
     });


     // View details of an image 
     app.get("/view-image", function (request, result) {

       database.collection("images").findOne({
         "_id": ObjectId(request.query._id)
        }, function (error1, image) {

          if (request.session.user_id) {
            getUser(request.session.user_id, function (user) {
                  result.render("view-image", {
                      "isLogin": true,
                      "query": request.query,
                      "image": image,
                      "user": user
                  });
            });
          } else {
              result.render("view-image", {
                  "isLogin": false,
                  "query": request.query,
                  "image": image
              });
          }
        });
    });


    // comments

    app.post("/do-comment", function (request, result) {
      if (request. session.user_id) {
          var comment = request.body.comment;
          var _id = request.body._id;
                        
          getUser(request.session.user_id, function (user) {
              delete user.password;

              database.collection("images").findOneAndUpdate({
                  "_id": ObjectId(_id) 
              }, {
                  $push: {
                      "comments": {
                          "_id":  ObjectId(), 
                          "user": user,
                          "comment": comment,
                          "createdAt": new Date().getTime()   
                    }
                  }
              }, function (error1, data) {
                  result.redirect("/view-image?_id=" + _id + "&message=success#comments");
                });
              });
      } else {
          result.redirect("/view-image?_id=" + _id + "&error=not_login#comments");
      }
    });


    app.post("/view-image/delete/:id", function(request, result){

      if (request.session.user_id) {
        var imageUserID = request.body._id;
                      
        getUser(request.session.user_id, function (user) {
            delete user.password;
            if (user._id.toString() == imageUserID){
              database.collection("images").deleteOne({
                "_id": ObjectId(request.params.id) 
                  }, function (error1, data) {
                    result.redirect("/");
                  }
              );
              console.log("sucess ");
            }
            else{
              result.redirect("/view-image?_id=" + request.params.id + "&error=not_user#comments");
            }


         });

      } else {
        result.redirect("/view-image?_id=" + request.params.id + "&error=not_login#comments");
      }
  });

  });
});

 